//
//  WIPoint.h
//  Wolt Intw
//
//  Created by Lukáš Fečke on 02/02/2017.
//  Copyright © 2017 Lukáš Fečke. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WIPoint : NSObject

@property (nonatomic,assign) int x;
@property (nonatomic,assign) int y;
@property (nonatomic,assign) int distance;
@property (nonatomic,assign) bool isWall;
@property (nonatomic,assign) bool isOnRoute;
@property (nonatomic,strong) NSString *mark;

- (id)initWithX:(int)x andWithY:(int)y andWithMark:(NSString*)mark;
- (id)initByCopyFromPoint:(WIPoint*)point;
- (bool)isEqual:(WIPoint*)point;
- (bool)hasGreaterDistanceThan:(WIPoint*)point;
- (bool)shouldGoHereFrom:(WIPoint*)point;
- (bool)isBetterWayThan:(WIPoint*)point;

@end
