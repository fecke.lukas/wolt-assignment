//
//  WIPoint.m
//  Wolt Intw
//
//  Created by Lukáš Fečke on 02/02/2017.
//  Copyright © 2017 Lukáš Fečke. All rights reserved.
//

#import "WIPoint.h"

@implementation WIPoint

- (id)initWithX:(int)x andWithY:(int)y andWithMark:(NSString*)mark
{
    if (self = [super init])
    {
        self.x = x;
        self.y = y;
        self.isWall = NO;
        self.mark = mark;
        self.distance = INT_MAX;
    }
    
    return self;
}

- (id)initByCopyFromPoint:(WIPoint*)point
{
    if (self = [super init])
    {
        self.x = point.x;
        self.y = point.y;
        self.mark = point.mark;
        self.isWall = point.isWall;
        self.distance = point.distance;
    }
    
    return self;
}

- (bool)isEqual:(WIPoint*)point
{
    if (self.x == point.x && self.y == point.y)
    {
        return YES;
    }
    
    return NO;
}

- (void)setMark:(NSString *)mark
{
    if ([mark isEqualToString:@"#"])
    {
        self.isWall = YES;
    }
    
    _mark = mark;
}

- (bool)shouldGoHereFrom:(WIPoint*)point
{
    return !self.isWall && [self hasGreaterDistanceThan:point];
}

- (bool)isBetterWayThan:(WIPoint*)point
{
    if (point == nil)
    {
        return YES;
    }
    else if (self.distance <= point.distance)
    {
        return YES;
    }
    return NO;
}

- (bool)hasGreaterDistanceThan:(WIPoint*)point
{
    if (self.distance > point.distance + 1)
    {
        return YES;
    }
    
    return NO;
}

@end
