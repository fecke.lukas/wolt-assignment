//
//  ViewController.h
//  Wolt Intw
//
//  Created by Lukáš Fečke on 02/02/2017.
//  Copyright © 2017 Lukáš Fečke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WIRouteFinder.h"

@interface ViewController : UIViewController

@property(nonatomic,strong) id<WIRouteFinderDelegate> delegate;

@end

