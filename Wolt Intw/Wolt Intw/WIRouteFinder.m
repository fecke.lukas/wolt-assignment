//
//  WIRouteFinder.m
//  Wolt Intw
//
//  Created by Lukáš Fečke on 02/02/2017.
//  Copyright © 2017 Lukáš Fečke. All rights reserved.
//

#import "WIPoint.h"
#import <UIKit/UIKit.h>
#import "WIRouteFinder.h"


@implementation WIRouteFinder

- (id)init
{
    if (self = [super init])
    {
        self.recursionCounter = 0;
    }
    
    return self;
}

- (int)getRC
{
    return self.recursionCounter;
}

- (NSString*)printMap:(NSMutableArray*)map
{
    NSString *mapString = @"";
    
    for (NSMutableArray *array in map)
    {
        for (NSString *cell in array)
        {
            mapString = [NSString stringWithFormat:@"%@ %@", mapString, cell];
        }
        mapString = [NSString stringWithFormat:@"%@\n", mapString];
    }
    
    return mapString;
}

- (NSString*)printRoute
{
    if (self.pointMap == nil)
    {
        return @"Use GPS first";
    }
    
    NSString *routeEstimationString = @"";
    
    for (NSMutableArray *array in self.pointMap)
    {
        for (WIPoint *point in array)
        {
            routeEstimationString = [NSString stringWithFormat:@"%@ %@", routeEstimationString, point.mark];
        }
        routeEstimationString = [NSString stringWithFormat:@"%@\n", routeEstimationString];
    }
    
    return routeEstimationString;
}

- (NSString*)getRouteFromTextViewMap:(UITextView*)textView
{
    NSMutableArray *map = [[NSMutableArray alloc] init];
    NSMutableArray *mapText = [[NSMutableArray alloc] init];
    
    NSString *textViewString = [textView text];//[[textView text] stringByReplacingOccurrencesOfString:@" " withString:@""];
    mapText = (NSMutableArray*)[textViewString componentsSeparatedByString:@"\n"];
    [mapText removeObject:[mapText lastObject]];
    
    for (NSString *line in mapText)
    {
        NSLog(@"%@", line);
        [map addObject:(NSMutableArray*)[line componentsSeparatedByString:@" "]];
    }
    
    return [self findRoute:map];
}

- (NSString*)findRoute:(NSMutableArray*)map
{
    self.recursionCounter = 0;
    
    //map with points
    self.pointMap = [[NSMutableArray alloc] init];
    
    int height = (int)[map count];
    int width = (int)[(NSMutableArray*)[map firstObject] count];

    WIPoint *courier;
    WIPoint *customer;
    
    for (int i = 0; i < height; i++)
    {
        [self.pointMap addObject:[[NSMutableArray alloc] init]];
        
        for (int j = 0; j < width; j++)
        {
            WIPoint *point = [[WIPoint alloc] initWithX:j andWithY:i andWithMark:map[i][j]];
            if ([map[i][j] isEqualToString:@"@"])
            {
                courier = point;
            }
            else if ([map[i][j] isEqualToString:@"C"])
            {
                customer = point;
                customer.distance = 0;
            }
            [(NSMutableArray*)self.pointMap[i] addObject:point];
        }
    }
    
    
    
    [self recursiveEvaluationWithStart:customer andWithEnd:courier];
    NSString *mapString = @"";
    NSString *routeEstimationString = @"";
    
    for (NSMutableArray *array in self.pointMap)
    {
        for (WIPoint *point in array)
        {
            NSString *distance = [NSString stringWithFormat:@"%d", point.distance];
            mapString = [NSString stringWithFormat:@"%@ %@", mapString, point.mark];
            routeEstimationString = [NSString stringWithFormat:@"%@ %@", routeEstimationString, point.isWall ? @"#" : distance];
        }
        mapString = [NSString stringWithFormat:@"%@\n", mapString];
        routeEstimationString = [NSString stringWithFormat:@"%@\n", routeEstimationString];
    }
    
    
    NSLog(@"\n\n%@\n\n%@", mapString, routeEstimationString);
    
    return [self getRoutefromPoint:courier toPoint:customer];
}

- (void)recursiveEvaluationWithStart:(WIPoint*)start andWithEnd:(WIPoint*)end
{
    int height = (int)[self.pointMap count];
    int width = (int)[(NSMutableArray*)[self.pointMap firstObject] count];
    
    if ([start isEqual:end] || start.x < 0 || start.y < 0 || start.x >= width || start.y >= height)
    {
        return;
    }
    self.recursionCounter++;
    
    int upY = start.y - 1;
    int downY = start.y + 1;
    int leftX = start.x - 1;
    int rightX = start.x + 1;
    
    WIPoint *rightPoint = rightX >= width ? nil : (WIPoint*)self.pointMap[start.y][rightX];
    WIPoint *downPoint = downY >= height ? nil : (WIPoint*)self.pointMap[downY][start.x];
    WIPoint *upPoint = upY < 0 ? nil : (WIPoint*)self.pointMap[upY][start.x];
    WIPoint *leftPoint = leftX < 0 ? nil : (WIPoint*)self.pointMap[start.y][leftX];
    
    
    
    //check RIGHT cell
    if (rightPoint != nil && [rightPoint shouldGoHereFrom:start])
    {
        rightPoint.distance = start.distance + 1;
        [self recursiveEvaluationWithStart:rightPoint andWithEnd:end];
    }
    
    //check DOWN cell
    if (downPoint != nil && [downPoint shouldGoHereFrom:start])
    {
        downPoint.distance = start.distance + 1;
        [self recursiveEvaluationWithStart:downPoint andWithEnd:end];
    }
    
    //check LEFT cell
    if (leftPoint != nil && [leftPoint shouldGoHereFrom:start])
    {
        leftPoint.distance = start.distance + 1;
        [self recursiveEvaluationWithStart:leftPoint andWithEnd:end];
    }
    
    //check UP cell
    if (upPoint != nil && [upPoint shouldGoHereFrom:start])
    {
        upPoint.distance = start.distance + 1;
        [self recursiveEvaluationWithStart:upPoint andWithEnd:end];
    }
    
}

- (NSString*)getRoutefromPoint:(WIPoint*)start toPoint:(WIPoint*)end
{
    NSLog(@"\n\n%@\n\n", [self printRoute]);
    int height = (int)[self.pointMap count];
    int width = (int)[(NSMutableArray*)[self.pointMap firstObject] count];
    
    if ([start isEqual:end] || start.x < 0 || start.y < 0 || start.x >= width || start.y >= height)
    {
        return [NSString stringWithFormat:@"end recursion: %d H x W: %d", self.recursionCounter, height*width];
    }
    
    if ([start.mark isEqualToString:@"."])
    {
        [start setMark:@"+"];
    }
    
    int upY = start.y - 1;
    int downY = start.y + 1;
    int leftX = start.x - 1;
    int rightX = start.x + 1;
    
    WIPoint *rightPoint = rightX >= width ? nil : (WIPoint*)self.pointMap[start.y][rightX];
    WIPoint *downPoint = downY >= height ? nil : (WIPoint*)self.pointMap[downY][start.x];
    WIPoint *upPoint = upY < 0 ? nil : (WIPoint*)self.pointMap[upY][start.x];
    WIPoint *leftPoint = leftX < 0 ? nil : (WIPoint*)self.pointMap[start.y][leftX];
    
    if ([rightPoint isBetterWayThan:leftPoint]
        && [rightPoint isBetterWayThan:upPoint]
        && [rightPoint isBetterWayThan:downPoint])
    {
        return [NSString stringWithFormat:@"r,%@", [self getRoutefromPoint:rightPoint toPoint:end]];
    }
    else if ([leftPoint isBetterWayThan:rightPoint]
             && [leftPoint isBetterWayThan:upPoint]
             && [leftPoint isBetterWayThan:downPoint])
    {
        return [NSString stringWithFormat:@"l,%@", [self getRoutefromPoint:leftPoint toPoint:end]];
    }
    else if ([upPoint isBetterWayThan:leftPoint]
             && [upPoint isBetterWayThan:rightPoint]
             && [upPoint isBetterWayThan:downPoint])
    {
        return [NSString stringWithFormat:@"u,%@", [self getRoutefromPoint:upPoint toPoint:end]];
    }
    else if ([downPoint isBetterWayThan:leftPoint]
             && [downPoint isBetterWayThan:upPoint]
             && [downPoint isBetterWayThan:rightPoint])
    {
        return [NSString stringWithFormat:@"d,%@", [self getRoutefromPoint:downPoint toPoint:end]];
    }
    
    return @"end";
}

@end
