//
//  WIRouteFinder.h
//  Wolt Intw
//
//  Created by Lukáš Fečke on 02/02/2017.
//  Copyright © 2017 Lukáš Fečke. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WIRouteFinderDelegate <NSObject>

@required
- (NSString*)getRouteFromTextViewMap:(UITextView*)textView;
- (NSString*)findRoute:(NSMutableArray*)map;
- (NSString*)printMap:(NSMutableArray*)map;
- (NSString*)printRoute;
- (int)getRC;


@end

@interface WIRouteFinder : NSObject <WIRouteFinderDelegate>

- (id)init;
@property(nonatomic,assign) int recursionCounter;
@property(nonatomic,strong) NSMutableArray* pointMap;

@end
